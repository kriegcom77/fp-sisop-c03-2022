#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <stdbool.h>

#define PORT 4443

typedef struct {
    char nama[500];
    char sandi[500];
} User;

bool periksaUser(char *nama, char *sandi){
    FILE *fp;
    User user;
    int id;
    fp = fopen("../database/databases/user.dat","rb");
    while(true){
        fread(&user, sizeof(user), 1, fp);
        if(strcmp(user.nama, nama) == 0)
            if(strcmp(user.sandi, sandi) == 0)
                return true;
        if(feof(fp)) break;
    }
    fclose(fp);
    printf("Kamu tidak diizinkan masuk\n");
    return false;

}

void writelog(char *command, char *nama){
    time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

    char buffer[1000];

    FILE *fp;
    char loc[1000];
    snprintf(loc, sizeof(loc), "../database/log/log%s.log", nama);
    fp = fopen(loc, "ab");

    sprintf(buffer, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, command);
    fputs(buffer, fp); fclose(fp);
    return;
}

int main(){

    return 0;
}

