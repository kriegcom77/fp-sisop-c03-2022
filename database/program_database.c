#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>

#define PORT 4443

typedef struct{
    char nama[500];
    char sandi[500];
} User;

typedef struct{
    char basis_data[500];
    char nama[500];
}UserDatabase;

typedef struct{
    int banyak_col;
    char tipe_data[100][500];
    char data[100][500];
}Table;

void buatUser(char *nama, char *sandi){
    User user;
    // copy nama ke user dan password
    strcpy(user.nama, nama); strcpy(user.sandi, sandi);
    // Buka FILE
    FILE *fp;
    char *file_name = "databases/user.dat";
    fp = fopen(file_name, "ab"); fwrite(&user, sizeof(user), 1, fp);
    fclose(fp);
}

bool periskaEksistensiUser(char *nama){
    FILE *fp;
    User user;
    fp = fopen("../database/databases/user.dat","rb");
    while(true){
        fread(&user, sizeof(user), 1, fp);
        // cek ketersediaan nama di database
        if(strcmp(user.nama, nama) == 0) return true;
        if(feof(fp)) break;
    }
    fclose(fp);
    return false;
} 

void beriPermission(char *nama, char *basis_data){
    UserDatabase user;
    strcpy(user.nama, nama);
    strcpy(user.basis_data, basis_data);
    char *file_name = "databases/permission.dat";
    
    FILE *fp;
    fp = fopen(file_name, "ab");
    fwrite(&user, sizeof(user), 1, fp);
    fclose(fp);
}

bool periksaUserDatabase(char *nama, char *basis_data){
    FILE *fp;
    UserDatabase user;
    fp = fopen("../database/databases/permission.dat","rb");
    while(true){
        fread(&user, sizeof(user), 1, fp);
        // periksa apakah user ada
        if(strcmp(user.nama, nama) == 0)
            if(strcmp(user.basis_data, basis_data) == 0) return true;
        if(feof(fp)) break;
    }
    fclose(fp);
    return false;
}

int cariKolom(char *tabel, char *kolom){
    FILE *fp;
    Table user;
    fp = fopen(tabel, "rb");
    fread(&user, sizeof(user), 1, fp);

    // default index
    int ind = -1;
    // traversal dengan loop
    for(int i=0; i<user.banyak_col; i++){
        if(strcmp(user.data[i], kolom) == 0) ind = i; // index ditemukan
    }
    if(feof(fp)) return -1;
    fclose(fp);
    return ind;
}

int hapusKolom(char *tabel, int ind){
    FILE *fp, *fp1;
    Table user;
    fp = fopen(tabel, "rb"); fp1 = fopen("temp", "wb");
    while(true){
        fread(&user, sizeof(user), 1, fp);
        if(feof(fp)) break;
        Table userCopy;
        int it;
        for(int i=0; i<user.banyak_col; i++){
            if(i == ind) continue;
            strcpy(userCopy.tipe_data[it], user.tipe_data[i]);
            strcpy(userCopy.data[it], user.data[i]);
            it++;
        }
        userCopy.banyak_col = user.banyak_col;
        fwrite(&userCopy, sizeof(userCopy), 1, fp1);
    }
    fclose(fp); fclose(fp1);
    remove(tabel); rename("temp", tabel);
    return 0;
}

int updateKolom(char *table, int ind, char *replace){
    FILE *fp, *fp1;
    Table user;
    fp = fopen(table, "rb");
    fp1 = fopen("temp", "ab");
    int ind_ke = 0;
    while(true){
        fread(&user, sizeof(user), 1, fp);
        if(feof(fp)) break;
        Table userCopy;
        int it = 0;
        for(int i=0; i<user.banyak_col; i++){
            if(i == ind && ind_ke != 0)
                strcpy(userCopy.data[it], replace);
            else
                strcpy(userCopy.data[it], user.data[i]);
            
            // cetak 
            printf("%s\n", userCopy.data[it]); strcpy(userCopy.tipe_data[it], user.tipe_data[i]);
            printf("%s\n", userCopy.tipe_data[it]);
            it++;
        }
        userCopy.banyak_col = user.banyak_col;
        fwrite(&userCopy, sizeof(userCopy), 1, fp1);
        ind_ke++;
    }
    fclose(fp); fclose(fp1); remove(table); rename("temp", table);
    return 0;
}

int deleteTabelCondition(char *tabel, int ind, char *kolom, char *condition){
    int id, found = 0;
    FILE *fp, *fp1;
    Table user;
    fp = fopen(tabel, "rb"); fp1 = fopen("temp", "ab");
    int ind_ke = 0;

    while(true){
        found = 0;
        fread(&user, sizeof(user), 1, fp);
        if(feof(fp)) break;

        Table userCopy;
        int it = 0;
        for(int i=0; i<user.banyak_col; i++){
            if(i == ind && ind_ke != 0 && strcmp(user.data[i], condition) == 0)
                found = 1;
            
            strcpy(userCopy.data[it], user.data[i]); printf("%s\n", userCopy.data[it]);
            strcpy(userCopy.tipe_data[it], user.tipe_data[i]); printf("%s\n", userCopy.tipe_data[it]);
            it++;
        }
        userCopy.banyak_col = user.banyak_col;
        if(found != 1) fwrite(&userCopy, sizeof(userCopy), 1, fp1);
        ind_ke++;
    }
    fclose(fp); fclose(fp1); remove(tabel); rename("temp", tabel);
    return 0;
}

void writeLog(char *command, char *nama){
    time_t rawtime;
    struct tm *time_info; time(&rawtime); time_info = localtime(&rawtime);

    char buffer[1000];

    FILE *fp;
    fp = fopen("logUser.log", "ab");
    sprintf(buffer, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",time_info->tm_year + 1900, time_info->tm_mon + 1, time_info->tm_mday, time_info->tm_hour, time_info->tm_min, time_info->tm_sec, nama, command);

    fputs(buffer, fp);
    fclose(fp);
}

int main(){




    return 0;
}
